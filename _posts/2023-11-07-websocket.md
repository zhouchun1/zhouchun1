---
layout: post
title: WebSocket
subtitle: 双向传输
date: 2023-10-27
author: zhouchun
header-img: img/post-bg-6.jpg
tags:
  - 长连接
  - WebSocket
---

# 简介

传统的 http 请求，只能由客户端发起，而且只能和服务器一问一答，当需要服务器主动发送或者和服务器频繁交互的时候，就需要用到 webSocket

# 使用场景

WebSocket 适用于以下场景：

即时通信：WebSocket 提供了实时、双向的通信通道，可以用于构建即时聊天应用、实时游戏，直播或其他需要实时双向通信的应用。

实时数据更新：如果你的应用需要实时获取后端服务器的数据更新，而不需要通过轮询来获取，那么 WebSocket 是一个很好的选择。例如，股票市场数据、即时消息、实时位置更新等。

在线协作与共享：如果你的应用需要多个用户之间实时协作或共享数据，WebSocket 可以提供实时的数据同步。例如，实时编辑文档、白板协作工具、远程会议等。

通知与提醒：WebSocket 可以用于推送通知和提醒，将实时信息直接发送给客户端，而不需要客户端主动轮询服务器。例如，社交网络中的新消息通知、实时报警系统等。

总的来说，WebSocket 适用于需要实时、双向通信的场景，它相比传统的 HTTP 请求具有更低的延迟和带宽消耗，适合构建实时性较高的应用。

# 前端使用

```js
import { ref } from 'vue';
const ws = ref();

const connect = () => {
  ws.value = new WebSocket('ws://localhost:3005/ws');
  ws.value.addEventListener('open', () => {
    console.log('WebSocket open 可以发送消息了');
  });

  ws.value.addEventListener('message', ({ data: msg }) => {
    console.log('WebSocket message:', msg);
  });

  ws.value.addEventListener('error', () => {
    console.log('WebSocket error');
  });

  ws.value.addEventListener('close', () => {
    console.log('WebSocket close');
  });
};

const close = () => {
  ws.value.close();
};
```

# 后端使用 express-ws 插件

```js
const express = require('express');
const app = express();
const expressWs = require('express-ws');
// 将 WebSocket 服务混入 app，相当于为 app 添加 .ws 方法
expressWs(app);

app.ws('/ws', (ws, req) => {
  // ws.send  使用send方法向客户端发送数据
  ws.send('111');
  // 使用 on 方法监听事件
  //   message 事件表示从另一段（服务端）传入的数据
  ws.on('message', function (msg) {
    console.log(`receive message ${msg}`);
    ws.send(`这是服务端接受的消息${msg}`);
  });
  // close 事件表示客户端断开连接时执行的回调函数
  //  ws.on('close', function (e) {
  //   console.log('close connection')
  // })
});

// 监听端口
app.listen('3005', () => {
  console.log('服务已启动 localhost:3005');
});
```
