---
layout: post
title: 虚拟列表
subtitle: 只争对可视区域渲染，对非可视区域的数据不渲染或者部分渲染的一种渲染方式
date: 2023-10-25
author: zhouchun
header-img: img/post-bg-7.jpg
tags:
  - 虚拟列表
  - 动画帧节流优化
  - 二分法查找
  - ResizeObseve API
---

gitee 地址：[Carl/vue3+element-plus for demo (gitee.com)](https://gitee.com/zhouchun1/vue3-element-plus-for-demo)

# 什么是虚拟列表

只争对可视区域渲染，对非可视区域的数据不渲染或者部分渲染的一种渲染方式，用于解决大量数据渲染的问题

针对不分页的长列表，传统做法

时间分片，懒加载

虽然首次加载快，但是随着滚动条滑动，越来越多的新的内容加载，浏览器不断重绘重排，导致浏览器卡顿

# 实现思路

[![piV9rZQ.png](https://z1.ax1x.com/2023/10/25/piV9rZQ.png)](https://imgse.com/i/piV9rZQ)

1.使用一个大的 div，用来模拟所有数据，高度和所有数据的高度相同，然后给可视区域设置滚动，这样就可以模拟滚动条了

2.在容器内生成一个列表，列表内容数量，通过用可视区域的高度除以子集高度获取，向上取整
假如每个子集的高度都是不固定的，可以先使用一个相近的平均高度，定义一个高度缓存数组，缓存数组包括 height，index，top（子集上边框距离顶部的距离），bottom（子集下边框距离顶部的距离）

3.列表中展示的动态内容，通过起始下标和结束下标，从 data 中 slice 下来 4.通过滚动事件的 scrollTop，计算起始坐标，和结束下标，并在 update 函数等 dom 渲染完成后，实事更新高度缓存数组的 height，top，bottom
通过动画帧对滚动事件做节流优化，减少计算量
使用使用二分检索，查找 scrolltop 在哪个的 top 和 bottom 之间，获取 startIndex，加上列表需要展示数量得到 endIndex

4.初始把列表定位在容器的顶部，这时候滑动滚动条的时候，通过 translateY 偏移量,让可视区域永远展示 startIndex 到 endIndex 之间的数据,由于滑动时,startIndex 还没计算完成,可能会出现白屏的问题,这个可以通过增加列表上下的 buffer 来解决这个问题

# 遇到问题

问题 1:position 高度记录数组，通过累加高度和 scrollTop 比较得到 startIndex，当进度条滚动到数据比较大的时候 startindex 计算比较吃力，导致页面卡顿

解决：position 记录高度，和目标元素顶部和底部距离顶部的距离 ，通过二分法检索找到 startIndex
因为更新高度也比较消耗内存，所以更新前可以先进行比对，没有变化可以不用更新

问题 2:初始化的时候 onUpdated 函数不触发,导致首屏高度不更新到缓存高度数组

解决:可以在 onMounted 手动触发一遍更新

问题 3:列表中操作导致子集高度发生变化,如图片加载,高度缓存数组不会触发更新

解决:使用 ResizeObserve 监听元素的高度变化

# 具体实现

## 1.模板部分

```html
<template>
  <div class="container" :style="{ height: props.viewHeight + 'px' }" @scroll="optimizeScroll">
    <!-- list-fill盒子用来填充高度模拟滚动条位置  -->
    <div class="list-fill" :style="{ height: fillHeight + 'px' }"></div>
    <!-- 动态列表 -->
    <ul class="list" :style="{ transform: getTransForm() }">
      <li v-for="item in visibleList" class="item" :key="item.index" :id="item.index" ref="items">
        <slot name="item" :row="item"></slot>
      </li>
    </ul>
  </div>
</template>
```

## 2.样式

```scss
<style lang="scss" scoped>
.container {
  overflow: auto;
  position: relative;
  .list {
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
    list-style: none;
  }
}
</style>
```

## 3.接收 props

```js
// 获取可视区域的高度 viewHeight  单个item假设高度  itemHeight  列表数据 list {index }
const props = defineProps(['list', 'viewHeight', 'itemHeight']);
```

## 4.定义变量和计算属性

```js
// 监听器
const resizeObervers = ref();
//items dom
const items = ref();
// 可展示item个数
const num = Math.ceil(props.viewHeight / props.itemHeight);
// 前后缓存数量
const bufferNum = 5;
//起始坐标 和结束坐标    结束坐标 = 起始坐标 + 可展示item个数
const startIndex = ref(0);
const endIndex = computed(() => {
  return startIndex.value + num;
});
//缓存后的起始坐标和结束坐标
const startBufferIndex = computed(() => {
  return startIndex.value < bufferNum ? startIndex.value : startIndex.value - bufferNum;
});
const endBufferIndex = computed(() => {
  return endIndex.value + Math.min(props.list.length - endIndex.value, bufferNum);
});
//展示的数据
const visibleList = computed(() => {
  return props.list.slice(startBufferIndex.value, endBufferIndex.value);
});
// 记录高度信息
const positions = reactive(
  props.list.map(v => {
    return {
      index: v.index,
      height: props.itemHeight,
      top: v.index * props.itemHeight,
      bottom: v.index * props.itemHeight + props.itemHeight
    };
  })
);
// 填充高度  所有li的总高度
const fillHeight = computed(() => {
  return positions.reduce((a, v) => a + v.height, 0);
});
// 保持列表在可视区域 所以给列表设置偏移
const startOffset = ref(0);
```

## 5.滚动事件,动画帧节流优化

```js
// 利用请求动画帧做了一个节流优化
const current = ref(0);
const optimizeScroll = e => {
  const now = Date.now();
  /**
   * 这里的等待时间不宜设置过长，不然会出现滑动到空白占位区域的情况
   * 因为间隔时间过长的话，太久没有触发滚动更新事件，下滑就会到padding-bottom的空白区域
   * 电脑屏幕的刷新频率一般是60HZ，渲染的间隔时间为16.6ms，我们的时间间隔最好小于两次渲染间隔16.6*2=33.2ms，一般情况下30ms左右，
   */
  if (now - current.value > 30) {
    current.value = now;
    // 重复调用scrollHandle函数，让浏览器在下一次重绘之前执行函数，可以确保不会出现丢帧现象
    window.requestAnimationFrame(() => handleScroll(e));
  }
};
//滚动事件
const handleScroll = e => {
  // 滚动位置
  let scrollTop = e.target.scrollTop;

  // 查找起始坐标
  if (scrollTop == 0) {
    startIndex.value = 0;
    startOffset.value = 0;
  } else {
    // 使用二分法查找startIndex
    startIndex.value = searchIndex(scrollTop);
    startOffset.value = positions[startBufferIndex.value].top;
  }
  console.log(startIndex.value, scrollTop, 'startIndex');
};
```

## 6.二分法查找

```js
const searchIndex = scrollTop => {
  return binarySearch(positions, scrollTop, (midValue, value) => {
    if (value < midValue.bottom && value > midValue.top) {
      return '相等';
    } else if (value > midValue.bottom) {
      return '大于中间值';
    } else {
      return '小于中间值';
    }
  });
};

// 二分查找核心算法
const binarySearch = function (list, value, compareFunc) {
  let start = 0;
  let end = list.length - 1;
  let midIndex = null;
  while (start <= end) {
    midIndex = Math.floor((start + end) / 2);
    const midValue = list[midIndex];
    const compareRes = compareFunc(midValue, value);
    // 一般情况是找不到完全相等的值，只能找到最接近的值
    if (compareRes === '相等') {
      return midIndex;
    }
    if (compareRes === '大于中间值') {
      start = midIndex + 1;
    } else if (compareRes === '小于中间值') {
      end = midIndex - 1;
    }
  }
  return midIndex;
};
```

## 7.ResizeObserve 监听，更新高度

```js
watchEffect(() => {
  resizeObervers.value?.disconnect();
  items.value && observer(items.value);
});

const observer = nodes => {
  // 监听元素的高度变化,更新positions
  resizeObervers.value = new ResizeObserver(entries => {
    updatePositions(entries.map(v => v.target));
  });
  nodes.forEach(el => resizeObervers.value.observe(el));
};

const updatePositions = async nodes => {
  await nextTick();
  if (!nodes) return;
  // 渲染完成后 用真实的高度替换假设高度 clientHeight
  nodes.forEach(node => {
    // node对应的position
    let position = positions.find(v => v.index == node.id);
    // 第一个只修改 height ，bottom 其余修改 height top bottom
    position.height = node.clientHeight;
    position.bottom =
      position.index > 0 ? positions[position.index - 1].bottom + position.height : position.height;
    position.top = position.index > 0 ? positions[position.index - 1].bottom : 0;
  });
  // 每次修改dom的高度，同步修改下面所有dom的top bottom
  for (let i = Number(nodes[nodes.length - 1].id) + 1; i < positions.length; i++) {
    positions[i].bottom = positions[i - 1].bottom + positions[i].height;
    positions[i].top = positions[i - 1].bottom;
  }
};
```

## 8.设置偏移量

```js
const getTransForm = () => {
  return `translateY(${startOffset.value}px)`;
};
```

# 参考博客

https://juejin.cn/post/6844903982742110216#comment

https://juejin.cn/post/6844904015440904200#heading-19

https://zhuanlan.zhihu.com/p/444778554
